package coronavirustechhandbook_offliner

import (
	"net/http"
)

const (
	pageSize = 100
)

var httpClient *http.Client

func init() {
	httpClient = &http.Client{}
}

// Offliner
type Offliner struct{}
