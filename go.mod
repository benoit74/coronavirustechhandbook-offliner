module gitlab.com/benoit74/coronavirustechhandbook-offliner

go 1.13

require (
	github.com/sirupsen/logrus v1.5.0
	github.com/spf13/cobra v0.0.6
)
