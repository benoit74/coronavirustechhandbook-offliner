package cmd

import (
	log "github.com/sirupsen/logrus"
	"github.com/spf13/cobra"
	offliner "gitlab.com/benoit74/coronavirustechhandbook_offliner"
)

var (
	exportCmd = &cobra.Command{
		Use:   "export",
		Short: "Bla",
		Long:  `Bla.`,
		Run: func(cmd *cobra.Command, args []string) {

			log.Debug("Running export")

			var offliner offliner.Offliner
			offliner.Do()

		},
	}
)

func init() {
	exportCmd.Flags().StringVarP(&verbosity, "verbosity", "v", "INFO", "Verbosity of logs sent to stderr (TRACE,DEBUG,INFO,WARN,ERROR,FATAL,PANIC)")
	rootCmd.AddCommand(exportCmd)
}
