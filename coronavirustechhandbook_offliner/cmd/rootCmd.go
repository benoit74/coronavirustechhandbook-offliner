package cmd

import (
	"strings"

	log "github.com/sirupsen/logrus"
	"github.com/spf13/cobra"
)

var (
	verbosity string

	rootCmd = &cobra.Command{
		Use:     "coronavirustechhandbook-offliner",
		Short:   "Offliner for the Coronavirus Tech Handbook",
		Long:    `Offliner for the Coronavirus Tech Handbook`,
		Version: "0.0.1",
	}
)

func prepare() {
	parseGenericFlags()
}

func parseGenericFlags() {

	switch strings.ToUpper(verbosity) {
	case "TRACE":
		log.SetLevel(log.TraceLevel)
	case "DEBUG":
		log.SetLevel(log.DebugLevel)
	case "INFO":
		log.SetLevel(log.InfoLevel)
	case "WARN":
		log.SetLevel(log.WarnLevel)
	case "ERROR":
		log.SetLevel(log.ErrorLevel)
	case "FATAL":
		log.SetLevel(log.FatalLevel)
	case "PANIC":
		log.SetLevel(log.PanicLevel)
	default:
		log.SetLevel(log.InfoLevel)
	}

}
func Execute() error {
	return rootCmd.Execute()
}
