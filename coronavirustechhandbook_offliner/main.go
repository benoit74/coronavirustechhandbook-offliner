package main

import (
	"os"

	log "github.com/sirupsen/logrus"
	"gitlab.com/benoit74/coronavirustechhandbook_offliner/coronavirustechhandbook_offliner/cmd"
)

func main() {
	log.SetOutput(os.Stderr)

	if err := cmd.Execute(); err != nil {
		log.Fatal("Fatal error during execution")
		os.Exit(1)
	}

	log.Debug("Command completed")
}
